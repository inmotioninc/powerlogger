/*
	Minimal_gpio.h
	Function declerations for Minimal_gpio.c
	Cerated by Björn Lager 2021-04-09

*/


#define PI_PUD_OFF  0
#define PI_PUD_DOWN 1
#define PI_PUD_UP   2


int gpioInitialise(void);

void gpioSetMode(unsigned gpio, unsigned mode);

int gpioGetMode(unsigned gpio);

void gpioSetPullUpDown(unsigned gpio, unsigned pud);

int gpioRead(unsigned gpio);

void gpioWrite(unsigned gpio, unsigned level);

