#define _XOPEN_SOURCE 700
#include <time.h>       /* time_t, struct tm, difftime, time, mktime */
#include <stdio.h>      /* printf */

#include <stdlib.h>
#include <string.h>

const char *config_filename = "powerlog.dat";

int lastWh;		// Global variable holding the totalt kWh. 
int currentHourWh;
int hourWh[25];
int currentDayWh;
int dayWh[8];
int currentWeekWh;
int weekWh[6];
int lowestHourWh;
long long lowestDate_long;
int highestHourWh;
long long highestDate_long;

/*
	Function: resetAllData 
	Description: This routine clears or set default value to all energy logging variables during program load 
	Input: 
	Output: 
	Returns: 
	* This routine clears or set default value to all variables during program load 
*/
void resetAllData(void)
{
	int i;
	
//	lastWh=0;
//	currentHourWh=0;

	for (i=0;i<25;i++)
		hourWh[i]=0;

	currentDayWh=0;
	for (i=0;i<8;i++)
		dayWh[i]=0;

//	currentWeekWh=0;
	for (i=0;i<6;i++)
		weekWh[i]=0;

	lowestHourWh=999;
    highestHourWh=0;
	lowestDate_long = 0;
	highestDate_long = 0;
}
/*
	Function: writeAllData 
	Description: Writes all program variables to data / configuration file. 
	Input: Pointer to configuration file-stream
	Output: 
	Returns: 1 if any errors or long long Unix epoch time when configuration was saved.
*/
long long writeAllData(FILE* minFil)
{
	int i;

	time_t now = time(NULL);

	printf("Sparar nuvarande tid i sekunder sedan Unix epoch:    %s \n", asctime(localtime(&now)));
	if (fprintf(minFil, "%lld\n", (long long) now) < 1)
	{
		printf("Fel vid skrivning av tidformat Unix epoch sekunder. \n ");
		return (1);		
	}
	printf("saved time:    %s", asctime(localtime(&now)));

	if (fprintf(minFil,"%i\n", lastWh) < 0)
	{
		printf("Fel vid skrivning av lastWh: %i \n", lastWh);
		return (1);
	}		

	fprintf(minFil, "%i\n",currentHourWh);
	for (i=0;i<25;i++)
		fprintf(minFil, "%i\n",hourWh[i]);
		
	fprintf(minFil, "%i\n",currentDayWh);
	for (i=0;i<8;i++)
		fprintf(minFil, "%i\n",dayWh[i]);

	fprintf(minFil, "%i\n",currentWeekWh);
	for (i=0;i<6;i++)
		fprintf(minFil, "%i\n",weekWh[i]);
		
	fprintf(minFil, "%i\n",lowestHourWh);
	fprintf(minFil, "%lld\n", lowestDate_long);

	fprintf(minFil, "%i\n",highestHourWh);			
	fprintf(minFil, "%lld\n"), highestDate_long;
	return (now);
}
/*
	Function: readAllData 
	Description: Reads all program variables from data / configuration file in case the program must be restarted. 
	Input: Pointer to configuration file-stream
	Output: 
	Returns: 1 if any errors or long long Unix epoch time when configuration was saved.
*/
long long readAllData(FILE* minFil)
{
	int i;
	time_t timeValue;
    long long restored_seconds=0;		
	
	if (fscanf(minFil, "%lld\n", &restored_seconds) !=1)
	{
		printf("Kunde inte läsa in tid i unix epoch sekunder från konfigurationsfil. \n");
		return (1);
	}
	timeValue = (time_t) restored_seconds;
	printf("Inläst tid: %s", asctime(localtime(&timeValue)));

	if (fscanf(minFil,"%i\n", &lastWh) < 1)
	{
		printf("Fel vid läsning av lastWh: %i \n", lastWh);
		return (1);
	}		

	fscanf(minFil, "%i\n",&currentHourWh);
	for (i=0;i<25;i++)
		fscanf(minFil, "%i\n",&hourWh[i]);
		
	fscanf(minFil, "%i\n",&currentDayWh);
	for (i=0;i<8;i++)
		fscanf(minFil, "%i\n",&dayWh[i]);

	fscanf(minFil, "%i\n",&currentWeekWh);
	for (i=0;i<6;i++)
		fscanf(minFil, "%i\n",&weekWh[i]);
		
	fscanf(minFil, "%i\n",&lowestHourWh);
	fscanf(minFil, "%lld\n", &lowestDate_long);
	
	fscanf(minFil, "%i\n",&highestHourWh);
	fscanf(minFil, "%lld\n", &highestDate_long);	

	return restored_seconds;
}
/*
	Function: createNewDataFile 
	Description: In case the data / configuration file used by the program cannot be found a new file is created. 
	Input: 
	Output: 
	Returns: 1 if any errors or a long long Unix epoch seconds time value. 
*/
long long createNewDataFile(void)
{
	long long actualTime=0;
	FILE* myFile = fopen (config_filename, "w");
	if (myFile == NULL)
	{
		printf ("Open file for write failed. ");
		return (1);
	}
	else
	{
		resetAllData();
		actualTime = writeAllData(myFile);
		fclose(myFile);
        if (actualTime == 1)
			return (1);
		else
			return (actualTime);
	}
}

/*
	Function: startup 
	Description: Main startup routine. Checks if a config/data file exists and if so get last stored energy values and the date & time it was stored.  
				Creates a new config/data file if it doesn't exist. 
	Input: 
	Output: 
	Returns: 1 if any errors or a long long Unix epoch seconds time value. 
*/

long long startup(void)
{
    long long restored_seconds=0;	

	// Open powerlog file for read.
	FILE *mfil = fopen(config_filename, "r");

	if (mfil == NULL) {
		// If it doesn't exists call a function to create the data in memory and write to file. 
		printf("Could not open powerlog.dat. Creating new file. \n");
		
		// Fake some values to test save & load function
		currentHourWh = 42;
		lastWh = 666;
		currentWeekWh = 242;

		restored_seconds = createNewDataFile();
		if (restored_seconds == 1)
			// if powerlog.data doesn't exists and cannot be created return error. 	
			return(1);
		else
			return(restored_seconds);		
	}
	else {
	 // If powerlog exists read all the data. 
		printf("Powerlog.dat exists. Read in all data.\n");
         
		restored_seconds = readAllData(mfil);
		if (restored_seconds == 1)
		{
				printf("Could not read powerlog.dat. \n");
				return (1);
		}
		printf("File read sucessfully! \n");
		printf ("Current hour Wh: %i \n" , currentHourWh);
		printf ("Current lastWh: %i \n" , lastWh);
		printf ("Current Week Wh: %i \n" , currentWeekWh);
		
	}
	return (restored_seconds);
}

int main ()
{
	time_t myTime;
	time_t loadedTime;
	struct tm * timeinfo;
	int month, weekday, h, m, s;
	long long restoredSeconds;
	double time_difference;

	time (&myTime);
	timeinfo = localtime ( &myTime );
	printf ("Current local time and date: %s \n", asctime(timeinfo));

	restoredSeconds = startup();
	if (restoredSeconds == 1) 
	{
		printf("Startup failed. \n");
		exit(99);
	}
	
/* Compare actual date & time with stored or created date & time. Alert the user about missing time since program last ran.
*/
	loadedTime = (time_t) restoredSeconds;
	time_difference = difftime(myTime, loadedTime);

	printf("Difference in seconds between actual and stored time: %f \n", time_difference);

	month = timeinfo->tm_mon;
	weekday = timeinfo->tm_wday;

	h = timeinfo->tm_hour; 
    m = timeinfo->tm_min;
    s = timeinfo->tm_sec;

	printf("Seconds: %i. Minutes: %i. Hours: %i \n", timeinfo->tm_sec, timeinfo->tm_min, timeinfo->tm_hour);
	printf("Month number: %i. Day in week number: %i \n", month, weekday);
	
/*	If long time since last startup write log that calibration must be done. 
*/	
/*  Loop

	Check if any energyvalue is stored for this month, week, day and hour. 
	If any of above is missing create those files and store present energy value. 

	increaseEnergyRegisterWithPulsesFromMeter
		ReadLed at least every 30ms.
		If two reads are 1 in a row, count that as a pulse. 
		Don't register a new pulse until we detect a 0. 
		
	get new time
	
	when time is 59:50 minutes store all files present energy register.  
	

*/
  return 0;
}