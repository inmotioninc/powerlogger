# Makefile for Powerlogger project.

.PHONY = all clean

CC = gcc

CFLAGS = -Wall -I.

LINKERFLAGS = -LM

all: powerlog powerlog.dat validate

powerlog: test_tid.o
	@echo "Linking and creating executable file.."
	$(CC) ${LINKERFLAGS} -o powerlog test_tid.o

test_tid.o: test_tid.c
	@echo "Creating object.."
	$(CC) $(CFLAGS) -c test_tid.c

clean:
	$(RM) powerlog *.o *~	
	$(RM) test_tid.o

powerlog.dat: powerlog
	@echo "Deleting powerlog.dat and running software first time."
	$(RM) powerlog.dat
	$(powerlog) 
	
validate: powerlog.dat
	$(powerlog)
	cat $(powerlog.dat)
	@echo "Validating complete."